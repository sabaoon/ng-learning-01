import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { BlogComponent } from './blog/blog.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { ContactComponent } from './contact/contact.component';
import { FormsModule } from '@angular/forms';
import { NglInterpolationsComponent } from './ngl-interpolations/ngl-interpolations.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    BlogComponent,
    FooterComponent,
    HeaderComponent,
    HomeComponent,
   routingComponents,
   ContactComponent,
   NglInterpolationsComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
 
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
